# Rover
The Mars Rover application keeps track of 2 Mars Rovers driving their way on a plateau on Mars. Both the rovers have a start position and drive a route on the platform. After the route is complete the app shows the final position and orientation of the rover.

# Setup
This app contains the source code for the Mars Rover application. The setup was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3. and extended to suit the specifications for the mars-rover application. 

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Run `ng test --code-coverage=true` to generate a full code coverage report

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
