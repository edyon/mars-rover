import { RoverPage } from './app.po';

describe('rover App', function() {
  let page: RoverPage;

  beforeEach(() => {
    page = new RoverPage();
  });

  it('should display start button', () => {
    page.navigateTo();
    expect(page.getStartButton()).toEqual('START');
  });
});
