import { browser, element, by } from 'protractor';

export class RoverPage {
  navigateTo() {
    return browser.get('/');
  }

  getStartButton() {
    return element(by.css('app-root button')).getText();
  }
}
