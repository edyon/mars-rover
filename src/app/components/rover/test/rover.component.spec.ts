/* tslint:disable:no-unused-variable */

import {TestBed, async, fakeAsync, tick} from '@angular/core/testing';

import {RoverComponent} from '../rover.component';
import {RouteService} from '../../../services/route.service';
import {Orientation} from '../../../models';

describe('RoverComponent', () => {
  let routeServiceMock;
  let routeSpy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        RoverComponent,
      ],
      providers: [
        RouteService
      ]
    });
    TestBed.compileComponents();
  });

  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(RoverComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));

  describe('handle moving driver 1', () => {
    let component;
    let fixture;

    beforeEach(() => {
      fixture = TestBed.createComponent(RoverComponent);
      fixture.detectChanges();

      component = fixture.debugElement.componentInstance;
      routeServiceMock = fixture.debugElement.injector.get(RouteService);
      routeSpy = spyOn(routeServiceMock, 'getRouteForRover').and.callThrough();
      routeSpy = spyOn(routeServiceMock, 'setDriverComplete').and.callThrough();
      routeSpy = spyOn(routeServiceMock, 'getStartPosition').and.callThrough();
      routeSpy = spyOn(routeServiceMock, 'getFinalPosition').and.callThrough();
      // routeSpy = spyOn(routeServiceMock, 'canDrive').and.returnValue(true);
    });

    it('should set the first position from route', fakeAsync(() => {
      component.driver = 1;
      component.ngAfterViewInit();
      fixture.detectChanges();

      expect(component.roverPosition.x).toBe(100);
      expect(component.roverPosition.y).toBe(300);
      expect(component.roverPosition.className).toBe('north');

      const route = component.getRoute();
      for (const step of route.steps) {
        tick(1000);
      }
    }));

    it('should set position after the route is complete', fakeAsync(() => {
      component.driver = 1;
      component.ngAfterViewInit();
      fixture.detectChanges();

      const route = component.getRoute();
      for (const step of route.steps) {
        tick(1000);
      }

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.roverPosition.x).toBe(100);
        expect(component.roverPosition.y).toBe(200);
        expect(component.roverPosition.className).toBe('north');
      });
    }));

    it('should have the desired output after the route is complete', fakeAsync(() => {
      component.driver = 1;
      component.ngAfterViewInit();
      fixture.detectChanges();

      const route = component.getRoute();
      for (const step of route.steps) {
        tick(1000);
      }

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const endPosition = routeServiceMock.getFinalPosition(component.roverPosition);
        expect(endPosition.x).toBe(1);
        expect(endPosition.y).toBe(3);
        expect(endPosition.orientation).toBe(Orientation.N);
      });
    }));
  });

  describe('handle moving driver 2', () => {
    let component;
    let fixture;

    beforeEach(() => {
      fixture = TestBed.createComponent(RoverComponent);
      fixture.detectChanges();

      component = fixture.debugElement.componentInstance;
      routeServiceMock = fixture.debugElement.injector.get(RouteService);
      routeSpy = spyOn(routeServiceMock, 'getRouteForRover').and.callThrough();
      routeSpy = spyOn(routeServiceMock, 'setDriverComplete').and.callThrough();
      routeSpy = spyOn(routeServiceMock, 'getStartPosition').and.callThrough();
      routeSpy = spyOn(routeServiceMock, 'getFinalPosition').and.callThrough();
      // routeSpy = spyOn(routeServiceMock, 'canDrive').and.returnValue(true);

      routeServiceMock.setDriverComplete(1, {});
    });

    it('should set the first position from route', fakeAsync(() => {
      component.driver = 2;
      component.ngAfterViewInit();
      fixture.detectChanges();

      expect(component.roverPosition.x).toBe(300);
      expect(component.roverPosition.y).toBe(200);
      expect(component.roverPosition.className).toBe('east');

      const route = component.getRoute();
      for (const step of route.steps) {
        tick(1000);
      }
    }));

    it('should set position after the route is complete', fakeAsync(() => {
      component.driver = 2;
      component.ngAfterViewInit();
      fixture.detectChanges();

      const route = component.getRoute();
      for (const step of route.steps) {
        tick(1000);
      }

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.roverPosition.x).toBe(500);
        expect(component.roverPosition.y).toBe(400);
        expect(component.roverPosition.className).toBe('east');
      });
    }));

    it('should have the desired output after the route is complete', fakeAsync(() => {
      component.driver = 2;
      component.ngAfterViewInit();
      fixture.detectChanges();

      const route = component.getRoute();
      for (const step of route.steps) {
        tick(1000);
      }

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const endPosition = routeServiceMock.getFinalPosition(component.roverPosition);
        expect(endPosition.x).toBe(5);
        expect(endPosition.y).toBe(1);
        expect(endPosition.orientation).toBe(Orientation.E);
      });
    }));
  });
});
