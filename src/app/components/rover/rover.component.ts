import {Component, Input, AfterViewInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

import {RouteService} from '../../services/route.service';
import {Action, Position, Route} from '../../models';

@Component({
  selector: 'app-rover',
  templateUrl: './rover.template.html',
  styleUrls: ['./_rover.css']
})

export class RoverComponent implements AfterViewInit {
  @Input() driver: number;
  private _driverSubscription: Subscription;
  private _step = 0;
  private _route: Route;
  roverPosition: Position = {};

  constructor(private _routeService: RouteService) {
    this._driverSubscription = this._routeService.driverState
      .subscribe(() => {
        this.startDriving();
      });
  }

  ngAfterViewInit() {
    this._route = this._routeService.getRouteForRover(`rover${this.driver}`);
    if (this._route) {
      this.roverPosition = this._routeService.getStartPosition(this._route.position);
      this.startDriving();
    }
  }

  getRoute(): Route {
    return this._route;
  }

  private startDriving() {
    if (this._routeService.canDrive(this.driver)) {
      this._step = 0;
      this.continueDriving();
    }
  }

  private continueDriving() {
    if (this._step < this._route.steps.length) {
      this.goToNextPosition();
    } else {
      const end = this._routeService.getFinalPosition(this.roverPosition);
      this._routeService.setDriverComplete(this.driver, end);
    }
  }

  private goToNextPosition() {
    setTimeout(() => {
      this.setNextPosition();
    }, 1000);
  }

  private setNextPosition() {
    const action: Action = this._route.steps[this._step];
    this._step++;
    this.roverPosition = this._routeService.getRoverPosition(this.roverPosition, action);
    this.continueDriving();
  }
}
