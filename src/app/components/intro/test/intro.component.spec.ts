/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {Router, RouterModule} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {By} from '@angular/platform-browser';
import {Location} from '@angular/common';

import {IntroComponent} from '../intro.component';

describe('IntroComponent', () => {
  let location, router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        IntroComponent
      ],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'intro', component: IntroComponent },
          { path: 'route', component: IntroComponent }
        ])
      ]
    });
    TestBed.compileComponents();
  });

  beforeEach(inject([Router, Location], (_router: Router, _location: Location) => {
    location = _location;
    router = _router;
  }));

  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(IntroComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));

  describe('should navigate to route screen', () => {


    it('should have a start button', async(() => {
      const fixture = TestBed.createComponent(IntroComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('button').textContent).toContain('Start');
    }));
  });

});
