import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./_app.css']
})
export class AppComponent {
  constructor() {
  }
}
