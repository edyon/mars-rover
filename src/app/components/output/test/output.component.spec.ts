/* tslint:disable:no-unused-variable */

import {TestBed, async, fakeAsync} from '@angular/core/testing';
import {OutputComponent} from '../output.component';
import {RouteService} from '../../../services/route.service';
import {Orientation} from '../../../models';
import {RoverComponent} from '../../rover/rover.component';

describe('OutputComponent', () => {
  let routeServiceMock;
  let routeSpy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        OutputComponent
      ],
      providers: [
        RouteService
      ]
    });
    TestBed.compileComponents();
  });

  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(OutputComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));


  describe('handle driver output', () => {
    let component;
    let fixture;

    beforeEach(() => {
      fixture = TestBed.createComponent(OutputComponent);
      fixture.detectChanges();

      component = fixture.debugElement.componentInstance;
      routeServiceMock = fixture.debugElement.injector.get(RouteService);
      routeSpy = spyOn(routeServiceMock, 'setDriverComplete').and.callThrough();
    });

    it('should set the output for driver 1', fakeAsync(() => {
      component.driver = 2;
      component.ngAfterViewInit();
      fixture.detectChanges();

      const position = {x: 2, y: 3, orientation: Orientation.N};
      routeServiceMock.setDriverComplete(component.driver, position);

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.driverInfo.driver).toBe(2);
        expect(component.driverInfo.position.x).toBe(2);
        expect(component.driverInfo.position.y).toBe(3);
        expect(component.driverInfo.position.orientation).toBe(Orientation.N);
      });
    }));
  });
});
