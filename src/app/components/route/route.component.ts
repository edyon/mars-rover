import {Component} from '@angular/core';

import {RouteService} from '../../services/route.service';

@Component({
  selector: 'app-route-component',
  templateUrl: './route.template.html',
  styleUrls: ['./_route.css']
})

export class RouteComponent {
  constructor(private _routeService: RouteService) {

  }
}
