/* tslint:disable:no-unused-variable */

import {TestBed, async} from '@angular/core/testing';

import {RouteComponent} from '../route.component';
import {RoverComponent} from '../../rover/rover.component';
import {RouteService} from '../../../services/route.service';
import {OutputComponent} from "../../output/output.component";

describe('RouteComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                RouteComponent,
                RoverComponent,
                OutputComponent,
            ],
            providers: [
                RouteService
            ]
        });
        TestBed.compileComponents();
    });

    it('should create the component', async(() => {
        const fixture = TestBed.createComponent(RouteComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));

    it('should have 2 rover components', async(() => {
        const fixture = TestBed.createComponent(RouteComponent);
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('app-rover')).toBeTruthy();
    }));


});
