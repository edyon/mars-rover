import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import {RouteService} from './services/route.service';
import {AppComponent} from './components/app/app.component';
import {IntroComponent} from './components/intro/intro.component';
import {RouteComponent} from './components/route/route.component';
import {RoverComponent} from './components/rover/rover.component';
import {OutputComponent} from './components/output/output.component';

const appRoutes: Routes = [
  {path: 'route', component: RouteComponent},
  {path: '**', component: IntroComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    RouteComponent,
    RoverComponent,
    OutputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    RouteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
