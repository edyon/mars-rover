import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

import {Route, Orientation, Action, Position, DriverInfo} from '../models';

@Injectable()
export class RouteService {
  private _driverManager: Subject<DriverInfo> = new Subject();
  driverState: Observable<DriverInfo>;

  private _routes: Object;
  private _driver = 1;

  constructor() {
    this.driverState = this._driverManager.asObservable();

    this._routes = {
      rover1: {
        position: {
          x: 1,
          y: 2,
          orientation: Orientation.N
        },
        steps: [Action.L, Action.M, Action.L, Action.M, Action.L, Action.M, Action.L, Action.M, Action.M]
      },
      rover2: {
        position: {
          x: 3,
          y: 3,
          orientation: Orientation.E
        },
        steps: [Action.M, Action.M, Action.R, Action.M, Action.M, Action.R, Action.M, Action.R, Action.R, Action.M]
      }
    };
  }

  getRouteForRover(name): Route {
    return this._routes[name];
  }

  setDriverComplete(driver: number, position: Position): void {
    this._driver = driver + 1;
    this._driverManager.next({driver: driver, position: position});
  }

  canDrive(driver: number): boolean {
    return this._driver === driver;
  }

  getRoverPosition(position: Position, action: Action): Position {
    switch (action) {
      case Action.L:
        position.orientation -= 90;
        position.orientation = position.orientation === -90 ? 270 : position.orientation;
        break;
      case Action.R:
        position.orientation += 90;
        position.orientation = position.orientation === 360 ? 0 : position.orientation;
        break;
      case Action.M:
        position.x = position.orientation === 90 ? position.x + 100 : position.orientation === 270 ? position.x - 100 : position.x;
        position.y = position.orientation === 180 ? position.y + 100 : position.orientation === 0 ? position.y - 100 : position.y;
        break;
    }

    position.className = this.getOrientationClass(position.orientation);
    return position;
  }

  getStartPosition(position: Position): Position {
    const start = {} as Position;
    start.x = (position.x * 100);
    start.y = 500 - (position.y * 100);
    start.orientation = position.orientation;
    start.className = this.getOrientationClass(position.orientation);
    return start;
  }

  getFinalPosition(position: Position): Position {
    const end = {} as Position;
    end.x = position.x / 100;
    end.y = (500 - position.y) / 100;
    end.orientation = position.orientation;
    end.className = this.getOrientationClass(position.orientation);
    return end;
  }

  private getOrientationClass(orientation: Orientation): string {
    let className;
    switch (orientation) {
      case Orientation.N:
        className = 'north';
        break;
      case Orientation.E:
        className = 'east';
        break;
      case Orientation.S:
        className = 'south';
        break;
      case Orientation.W:
        className = 'west';
        break;
    }

    return className;
  }
}
