/* tslint:disable:no-unused-variable */

import {TestBed, async, inject, fakeAsync, tick} from '@angular/core/testing';

import {RouteService} from '../route.service';
import {Action, Orientation} from '../../models';

describe('RouteService', () => {
  let routeService;

  beforeEach(() => {
    routeService = new RouteService();
  });

  describe('handle route for drivers', () => {
    it('should give a route for driver 1', () => {
      const route = routeService.getRouteForRover('rover1');
      expect(route).not.toBeUndefined();
      expect(route.position.x).toBe(1);
      expect(route.position.y).toBe(2);
      expect(route.position.orientation).toBe(Orientation.N);
    });

    it('should give a route for driver 2', () => {
      const route = routeService.getRouteForRover('rover2');
      expect(route).not.toBeUndefined();
      expect(route.position.x).toBe(3);
      expect(route.position.y).toBe(3);
      expect(route.position.orientation).toBe(Orientation.E);
    });

    it('should not give a route for other drivers', () => {
      const route = routeService.getRouteForRover('driver3');
      expect(route).toBeUndefined();
    });
  });

  describe('handle driver can drive', () => {

    it('should set the next driver if route is complete', () => {
      routeService.setDriverComplete();
    });

    it('should check if the driver is allowed to drive', () => {
      routeService.canDrive();
    });
  });

  describe('handle rover position', () => {

    describe('handle different start positions on the plateau', () => {

      it('should set the initial position for driver 1', () => {
        const start = {x: 1, y: 2, orientation: Orientation.N};
        const position = routeService.getStartPosition(start);
        expect(position.x).toBe(100);
        expect(position.y).toBe(300);
        expect(position.className).toBe('north');
      });

      it('should set the initial position for driver 2', () => {
        const start = {x: 3, y: 3, orientation: Orientation.E};
        const position = routeService.getStartPosition(start);
        expect(position.x).toBe(300);
        expect(position.y).toBe(200);
        expect(position.className).toBe('east');
      });

      it('should set the initial position for another driver', () => {
        const start = {x: 5, y: 5, orientation: Orientation.S};
        const position = routeService.getStartPosition(start);
        expect(position.x).toBe(500);
        expect(position.y).toBe(0);
        expect(position.className).toBe('south');
      });
    });

    describe('handle the next position based on an action', () => {
      describe('handle rotating counter clockwise', () => {
        const action = Action.L;

        it('should rotate from south to east', () => {
          const currentPosition = {x: 300, y: 500, orientation: Orientation.S};
          const position = routeService.getRoverPosition(currentPosition, action);
          expect(position.x).toBe(300);
          expect(position.y).toBe(500);
          expect(position.orientation).toBe(Orientation.E);
          expect(position.className).toBe('east');
        });
      });

      describe('handle rotating clockwise', () => {
        const action = Action.R;

        it('should rotate from south to west', () => {
          const currentPosition = {x: 300, y: 500, orientation: Orientation.S};
          const position = routeService.getRoverPosition(currentPosition, action);
          expect(position.x).toBe(300);
          expect(position.y).toBe(500);
          expect(position.orientation).toBe(Orientation.W);
          expect(position.className).toBe('west');
        });
      });

      describe('handle moving', () => {
        const action = Action.M;

        it('should move south', () => {
          const currentPosition = {x: 300, y: 500, orientation: Orientation.S};
          const position = routeService.getRoverPosition(currentPosition, action);
          expect(position.x).toBe(300);
          expect(position.y).toBe(600);
          expect(position.orientation).toBe(Orientation.S);
          expect(position.className).toBe('south');
        });
      });
    });

    describe('handle different end positions on the plateau', () => {
      it('should set the end position', () => {
        const end = {x: 500, y: 500, orientation: Orientation.W};
        const position = routeService.getFinalPosition(end);
        expect(position.x).toBe(5);
        expect(position.y).toBe(0);
        expect(position.orientation).toBe(Orientation.W);
        expect(position.className).toBe('west');
      });
    });

  });

});
